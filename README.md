# TESTE #

## O que é o teste? ##

O teste é constuido pela função básica da equipe de suporte que é o envio de uma campanha.

## Instruções ##

1. Baixe [esse pacote de arquivos](https://bitbucket.org/yves2mobile/teste-equipe-de-suporte/downloads/teste.zip) em seguida acesse o painel administrativo
2. A campanha estará nomeada no seguinte formato TESTE {SEU NOME} FINAL
3. Hora da diversão!

![Party time!](https://media.giphy.com/media/axu6dFuca4HKM/giphy.gif)

## Observações ##

1. Atente ao nome dos arquivos
2. Verifique se os criativos possuem a configuração da nossa biblioteca Rocket
3. Certifique-se que esteja tudo no peso correto
4. Garanta que as linhas criativas estejam cadastradas corretamente

### BOA SORTE ###
![Good luck](https://media.giphy.com/media/10AYkGR9M75nLW/giphy.gif)


